Name:           python-varlink
Version:        27.1.1
Release:        2
Summary:        Python implementation of the Varlink protocol
License:        ASL 2.0
URL:            https://github.com/varlink/%{name}
Source0:        https://github.com/varlink/%{name}/archive/%{version}/%{name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel python3-rpm-macros

%description
An implementation of the Varlink protocol with python module. For
server implementations use the varlink.Server class. For client
implementations use the varlink.Client class.

%package -n python3-varlink
Summary:        Python implementation of the Varlink protocol

%{?python_provide:%python_provide python3-varlink}
Obsoletes:      python-varlink

%description -n python3-varlink
An implementation of the Varlink protocol with python module. For
server implementations use the varlink.Server class. For client
implementations use the varlink.Client class.

%prep
%autosetup -n python-%{version} -p1

%build
%py3_build

%check
CFLAGS="%{optflags}" %{__python3} %{py_setup} %{?py_setup_args} check

%install
%py3_install

%files -n python3-varlink
%doc README.md LICENSE.txt
%{python3_sitelib}/*

%changelog
* Thu Feb 20 2020 Jiangping Hu <hujp1985@foxmail.com> - 27.1.1-2
- Package init
